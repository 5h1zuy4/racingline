# Racingline

### Displays a virtual line of the boat's path from a telemetry file.

## Options:

**Directory** + **Filename**: Path of the telemetry file.      
**Range**: Racing line within this distance (blocks) will be rendered. Decrease if laggy.       
**Left/Right Hue**: Shift the hue of the line to this angle (degrees) when left/right key was pressed. 