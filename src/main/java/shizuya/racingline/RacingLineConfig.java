package shizuya.racingline;

import java.io.File;

import me.shedaniel.autoconfig.annotation.Config;
import me.shedaniel.autoconfig.annotation.ConfigEntry.BoundedDiscrete;
import me.shedaniel.autoconfig.AutoConfig;
import me.shedaniel.autoconfig.ConfigData;
import net.fabricmc.loader.api.FabricLoader;

@Config(name = "racingline")
public class RacingLineConfig implements ConfigData {

    private boolean enabled;
    private String directory = FabricLoader.getInstance().getConfigDir().toString() + File.separator + "sboathud" + File.separator + "telemetry";
    private String filename = ".csv";
    private int range = 128;
    @BoundedDiscrete(min = 0, max = 360)
    private int leftHue = 45;
    @BoundedDiscrete(min = 0, max = 360)
    private int rightHue = 315;

    @Override
    public void validatePostLoad() {
    }
    
    public static RacingLineConfig getConfig() {
        return AutoConfig.getConfigHolder(RacingLineConfig.class).get();
    }

    public void toggle() {
        this.enabled = !this.enabled;
    }

    public boolean isEnabled() {
        return this.enabled;
    }

    public String getDirectory() {
        return this.directory;
    }

    public String getFilename() {
        return this.filename;
    }

    public int getRange() {
        return this.range;
    }

    public float getLeftHue() {
        return (float) this.leftHue / 360;
    }

    public float getRightHue() {
        return (float) this.rightHue / 360;
    }

}
