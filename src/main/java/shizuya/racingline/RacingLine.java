package shizuya.racingline;

import java.io.File;
import java.io.FileReader;
import java.awt.Color;
import java.io.BufferedReader;
import java.util.List;
import java.util.ArrayList;

import org.lwjgl.glfw.GLFW;
import me.shedaniel.autoconfig.AutoConfig;
import me.shedaniel.autoconfig.serializer.GsonConfigSerializer;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.option.KeyBinding;
import net.minecraft.client.util.InputUtil;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.math.Vec3d;
import me.x150.renderer.event.RenderEvents;
import me.x150.renderer.render.Renderer3d;

public class RacingLine implements ClientModInitializer {

    private final KeyBinding TOGGLE = KeyBindingHelper.registerKeyBinding(new KeyBinding("Toggle", InputUtil.Type.KEYSYM, GLFW.GLFW_KEY_MINUS, "racingline"));

    public static MinecraftClient client = null;
    private static String fileLoaded = "";
    private static List<Vec3d> posList = new ArrayList<Vec3d>();
    private static List<Color> colorList = new ArrayList<Color>();
    
    @Override
    public void onInitializeClient() {
        client = MinecraftClient.getInstance();
		AutoConfig.register(RacingLineConfig.class, GsonConfigSerializer::new);
        ClientTickEvents.END_CLIENT_TICK.register(this::onEndClientTick);
        RenderEvents.WORLD.register(RacingLine::world);
    }

    private void onEndClientTick(MinecraftClient client) {
        if (TOGGLE.wasPressed()) {
            RacingLineConfig.getConfig().toggle();
        }
        if (RacingLineConfig.getConfig().isEnabled()) {
            if (RacingLineConfig.getConfig().getFilename() != fileLoaded) {
                loadTelemetry();
                fileLoaded = RacingLineConfig.getConfig().getFilename();
            }
        }
    }

    private void loadTelemetry() {
        posList.clear();
        colorList.clear();
        try {
            BufferedReader br = new BufferedReader(new FileReader(RacingLineConfig.getConfig().getDirectory() + File.separator + RacingLineConfig.getConfig().getFilename()));
            for (String line = br.readLine(); line != null; line = br.readLine()) {
                if (line.contains("speed")) continue;
                String[] row = line.split(",");
                double x = Double.parseDouble(row[2]);
                double y = Double.parseDouble(row[4]) + 0.01;
                double z = Double.parseDouble(row[3]);
                posList.add(new Vec3d(x, y, z));
                double steering = Double.parseDouble(row[6]);
                double throttle = Double.parseDouble(row[7]);
                float h = 0f;
                float s = 1f;
                float b = 0f;
                if (steering == 0) {
                    s = 0f;
                } else if (steering > 0) {
                    h = RacingLineConfig.getConfig().getLeftHue();
                } else {
                    h = RacingLineConfig.getConfig().getRightHue();
                }
                if (throttle == 0) {
                    b = 0.5f;
                } else if (throttle > 0) {
                    b = 1f;
                } else {
                    b = 0.25f;
                }
                colorList.add(Color.getHSBColor(h, s, b));
            }
            br.close();
        }
        catch (Exception e) {
        }
    }

    public static void world(MatrixStack stack) {
        if (RacingLineConfig.getConfig().isEnabled()) {
            int i = 0;
            for (Vec3d pos : posList) {
                if (i == 0) {
                    ++i;
                    continue;
                }
                Color c =  colorList.get(i);
                Vec3d last = posList.get(i - 1);
                ++i;
                // OutlineFramebuffer.useAndDraw(() -> {Renderer3d.renderLine(stack, c, last, pos);}, RacingLineConfig.getConfig().getWidth(), c, c);
                if (last.distanceTo(new Vec3d(client.player.getX(), client.player.getY(), client.player.getZ())) > RacingLineConfig.getConfig().getRange()) continue;
                Renderer3d.renderLine(stack, c, last, pos);
            }
        }
    }
}
